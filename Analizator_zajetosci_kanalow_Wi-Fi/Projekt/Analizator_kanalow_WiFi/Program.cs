﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Analizator_kanalow_WiFi
{
    internal class Program
    {
        public static Dictionary<Wlan.Dot11PhyType, string> PhyTypeToName = new Dictionary<Wlan.Dot11PhyType, string>
        {
            {Wlan.Dot11PhyType.ERP, "802.11g"},
            {Wlan.Dot11PhyType.HT, "802.11n"},
            {Wlan.Dot11PhyType.VHT, "802.11ac"}
        };

        // Ilość zajętych kanałów w górę i w dół.
        public static Dictionary<Wlan.Dot11PhyType, int> PhyTypeToBandwidth = new Dictionary<Wlan.Dot11PhyType, int>
        {
            {Wlan.Dot11PhyType.ERP, 2},
            {Wlan.Dot11PhyType.HT, 4},
            {Wlan.Dot11PhyType.VHT, 4}
        };

        public static Dictionary<uint, int> FreqToChanNum = new Dictionary<uint, int>
        {
            {2412000, 1},
            {2417000, 2},
            {2422000, 3},
            {2427000, 4},
            {2432000, 5},
            {2437000, 6},
            {2442000, 7},
            {2447000, 8},
            {2452000, 9},
            {2457000, 10},
            {2462000, 11},
            {2467000, 12},
            {2472000, 13}
            //{2484000, 14}
        };

        private static string GetStringForSSID(Wlan.Dot11Ssid ssid)
        {
            return Encoding.ASCII.GetString(ssid.SSID, 0, (int) ssid.SSIDLength);
        }

        private static void Main(string[] args)
        {
            var ChanUsage = new Dictionary<int, double>
            {
                {-3, 0},
                {-2, 0},
                {-1, 0},
                {0, 0},
                {1, 0},
                {2, 0},
                {3, 0},
                {4, 0},
                {5, 0},
                {6, 0},
                {7, 0},
                {8, 0},
                {9, 0},
                {10, 0},
                {11, 0},
                {12, 0},
                {13, 0},
                {14, 0},
                {15, 0},
                {16, 0},
                {17, 0}
            };

            var client = new WlanClient();
            foreach (var wlanIface in client.Interfaces)
            {
                var networks = wlanIface.GetAvailableNetworkList(0);
                foreach (var network in networks)
                {
                    Console.WriteLine("Znaleziono sieć z SSID: {0}", GetStringForSSID(network.dot11Ssid));
                    //Console.WriteLine("Moc {0} %", network.wlanSignalQuality);
                    var test = wlanIface.GetNetworkBssList(network.dot11Ssid, network.dot11BssType,
                        network.securityEnabled);

                    foreach (var bss in test)
                    {
                        Console.WriteLine("Częstotliwość: {0} MHz, Moc: {1} dBm / {2:E3} mW, Kanał: {3}, Typ: {4}",
                            bss.chCenterFrequency/1000, bss.rssi, Math.Pow(10, (bss.rssi/10.0)),
                            FreqToChanNum[bss.chCenterFrequency], PhyTypeToName[bss.dot11BssPhyType]);

                        for (var i = 1; i <= PhyTypeToBandwidth[bss.dot11BssPhyType]; i++)
                        {
                            ChanUsage[FreqToChanNum[bss.chCenterFrequency] - i] += Math.Pow(10, (bss.rssi/10.0));
                        }

                        ChanUsage[FreqToChanNum[bss.chCenterFrequency]] += Math.Pow(10, (bss.rssi/10.0));

                        for (var i = 1; i <= PhyTypeToBandwidth[bss.dot11BssPhyType]; i++)
                        {
                            ChanUsage[FreqToChanNum[bss.chCenterFrequency] + i] += Math.Pow(10, (bss.rssi/10.0));
                        }
                    }
                    Console.WriteLine();
                }
            }

            Console.WriteLine();
            Console.WriteLine("Zajętość poszczególnych kanałow:");

            foreach (var chan in FreqToChanNum)
            {
                Console.WriteLine("Kanał: {0} => Zajętość: {1:E3}", chan.Value, ChanUsage[chan.Value]);
            }

            PrintBestChannel(ChanUsage, false);
            PrintBestChannel(ChanUsage, true);
            Console.ReadLine();
        }

        private static void PrintBestChannel(Dictionary<int, double> ChanUsage, bool typeN)
        {
            Console.WriteLine();

            var bestChan = new List<int>();
            var factor = double.MaxValue;
            for (var c = 1; c < 14; c++)
            {
                double chanFactor = 0;

                if (typeN)
                {
                    chanFactor += ChanUsage[c - 4];
                    chanFactor += ChanUsage[c - 3];
                }
                chanFactor += ChanUsage[c - 2];
                chanFactor += ChanUsage[c - 1];
                chanFactor += ChanUsage[c];
                chanFactor += ChanUsage[c + 1];
                chanFactor += ChanUsage[c + 2];
                if (typeN)
                {
                    chanFactor += ChanUsage[c + 3];
                    chanFactor += ChanUsage[c + 4];
                }

                if (chanFactor < factor)
                {
                    bestChan.Clear();
                    bestChan.Add(c);
                    factor = chanFactor;
                }
                else if (Math.Abs(chanFactor - factor) < double.Epsilon)
                {
                    bestChan.Add(c);
                }
            }

            Console.Write(typeN ? "Najlepszy kanał do wyboru dla 802.11n: " : "Najlepszy kanał do wyboru dla 802.11g: ");

            foreach (var chan in bestChan)
            {
                Console.Write(chan + " ");
            }
            Console.WriteLine();
        }
    }
}